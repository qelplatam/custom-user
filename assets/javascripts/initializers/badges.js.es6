import { withPluginApi } from 'discourse/lib/plugin-api';
const ajax = require('discourse/lib/ajax').ajax;
const h = require("virtual-dom").h;


function initializePlugin(api) {
    const current_user = api.getCurrentUser();
    api.createWidget("top-users-list-widget", {
        tagName: "div.top-users-list",

        init() {
            let top_users = ajax(this.siteSettings.custom_user_api_url).then(response => {

                let wrapper = document.querySelector('ul.top-users-wrapper');
                response.data.forEach(user => {
                    let li = document.createElement("li");

                    let avatar = document.createElement("span");
                    avatar.classList.add("avatar");

                    let img = document.createElement("img");
                    img.setAttribute("src", user.avatar.replace(/\{size\}/, 120));
                    avatar.append(img);

                    let username = document.createElement("span");
                    username.classList.add("username");
                    username.innerText = user.username;

                    let score = document.createElement("span");
                    score.classList.add("score");
                    score.innerText = user.points + " pts"

                    li.append(avatar);
                    li.append(username);
                    li.append(score);
                    wrapper.append(li);
                });

            })
        },
        html(attrs, state) {
            return h("ul.top-users-wrapper");
        }
    });
    api.createWidget("current-user-score-widget", {
        tagName: "div.current-user-score",
        init() {
            ajax(this.siteSettings.custom_user_api_url + `/${current_user.id}` ).then(response => {
                let pts = document.querySelector("div.current-user-score span.qty");
                pts.innerText = response.data.points;
            });
        },
        html() {
            return h ("div.wrapper", [
                h("div.position", h('span')),
                h("div.user", [
                    h('div.avatar', h('img', {"src" : current_user.avatar_template.replace(/\{size\}/, 40) })),
                    h('div.username', [
                        h('h3.username', current_user.username),
                        h('p.title', current_user.title),
                    ])
                ]),
                h("div.score", [
                    h('span.qty'),
                    h('span.pts', "pts"),
                ]),
            ]);
        }
    });
}
export default {
    name: 'custom-badges',
    initialize(container) {
        withPluginApi('0.1', api => initializePlugin(api));
    }
};