import { withPluginApi } from 'discourse/lib/plugin-api';
const h = require("virtual-dom").h;

function initializePlugin(api) {
    api.createWidget("regras-preview-widget", {
        tagName: "div.regras-preview",
        init() {

        },
        html() {
            return [
                h("h1", Discourse.SiteSettings.custom_user_regras_title),
                h("p", Discourse.SiteSettings.custom_user_regras_preview),
                h("a", {
                    "href": "/regras",
                    "innerText": Discourse.SiteSettings.custom_user_regras_link
                })
            ];
        }
    });
}
export default {
    name: 'regras-preview',
    initialize(container) {
        withPluginApi('0.1', api => initializePlugin(api));
    }
};