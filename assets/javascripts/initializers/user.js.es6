import { withPluginApi } from 'discourse/lib/plugin-api';
const ajax = require('discourse/lib/ajax').ajax;
const h = require("virtual-dom").h;


function initializePlugin(api) {
    api.createWidget("profile-user-score-widget", {
        tagName: "div.profile-user-score",
        init(attrs) {
            ajax(this.siteSettings.custom_user_api_url + `/${attrs.username}` ).then(response => {
                let pts = document.querySelector("div.profile-user-score span.qty");
                let avatar = document.querySelector("div.profile-user-score div.avatar");

                let img = document.createElement("img");
                img.src = response.data.avatar.replace(/\{size\}/, 40);
                avatar.append(img);
                
                let username = document.querySelector("div.profile-user-score h3.username");
                username.innerText = attrs.username

                pts.innerText = response.data.points;
            });            
        },
        html() {
            return h ("div.wrapper", [
                h("div.position", h('span')),
                h("div.user", [
                    h('div.avatar'),
                    h('div.username', [
                        h('h3.username'),
                        h('p.title'),
                    ])
                ]),
                h("div.score", [
                    h('span.qty'),
                    h('span.pts', "pts"),
                ]),
            ]);
        }        
    });

}
export default {
    name: 'custom-user',
    initialize(container) {
        withPluginApi('0.1', api => initializePlugin(api));
    }
};